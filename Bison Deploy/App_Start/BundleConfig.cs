﻿using System.Web;
using System.Web.Optimization;

namespace Bison_Deploy
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new LessBundle("~/Less").Include("~/Less/layout.less"));
			bundles.Add(new ScriptBundle("~/Scripts").Include("~/Scripts/functions.js"));
		}
	}
}